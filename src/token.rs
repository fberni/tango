use crate::location::Location;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TokenKind {
    Plus,
    Minus,
    Star,
    Slash,
    Greater,
    Less,
    Equal,
    Bang,

    EqualEqual,
    BangEqual,
    GreaterEqual,
    LessEqual,

    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,

    Number(i64),
    Identifier,

    Semicolon,

    KwTrue,
    KwFalse,
    KwLet,
    KwIf,
    KwElse,
    KwWhile,

    Invalid,
    Eof,
}

impl TokenKind {
    pub fn precedence(self) -> i32 {
        match self {
            Self::EqualEqual | Self::BangEqual => 1,
            Self::Less | Self::LessEqual | Self::Greater | Self::GreaterEqual => 2,
            Self::Plus | Self::Minus => 3,
            Self::Star | Self::Slash => 4,
            _ => -1,
        }
    }

    pub fn make_ident_or_keyword(lexeme: &str) -> Self {
        match lexeme {
            "true" => Self::KwTrue,
            "false" => Self::KwFalse,
            "let" => Self::KwLet,
            "if" => Self::KwIf,
            "else" => Self::KwElse,
            "while" => Self::KwWhile,
            _ => Self::Identifier,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Token {
    pub kind: TokenKind,
    pub lexeme: String,
    pub location: Location,
}

impl Token {
    pub fn new(kind: TokenKind, lexeme: String, location: Location) -> Self {
        Self {
            kind,
            lexeme,
            location,
        }
    }
}
