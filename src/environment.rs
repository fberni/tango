use std::borrow::Borrow;
use std::cmp::Eq;
use std::collections::HashMap;
use std::hash::Hash;

#[derive(Debug, Clone)]
pub struct Environment<K, V> {
    stack: Vec<HashMap<K, V>>,
}

impl<K, V> Environment<K, V>
where
    K: Eq + Hash,
{
    pub fn new() -> Self {
        Self {
            stack: vec![HashMap::new()],
        }
    }

    pub fn new_scope(&mut self) {
        self.stack.push(HashMap::new());
    }

    pub fn close_scope(&mut self) {
        self.stack.pop();
    }

    pub fn add(&mut self, key: K, value: V) {
        self.stack.last_mut().unwrap().insert(key, value);
    }

    pub fn get<Q: ?Sized>(&self, key: &Q) -> Option<&V>
    where
        K: Borrow<Q>,
        Q: Eq + Hash,
    {
        for scope in self.stack.iter().rev() {
            if let value @ Some(_) = scope.get(key) {
                return value;
            }
        }
        None
    }

    pub fn set<Q: ?Sized>(&mut self, key: &Q, value: V)
    where
        K: Borrow<Q>,
        Q: Eq + Hash,
    {
        for scope in self.stack.iter_mut().rev() {
            if let Some(v) = scope.get_mut(key) {
                *v = value;
                return;
            }
        }
        unreachable!()
    }
}
