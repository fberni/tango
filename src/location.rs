use std::fmt;

#[derive(Debug, Clone, Copy)]
pub struct Location {
    pub offset: usize,
    pub line: usize,
    pub column: usize,
}

impl Default for Location {
    fn default() -> Self {
        Self {
            offset: 0,
            line: 1,
            column: 0,
        }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}", self.line, self.column)
    }
}
