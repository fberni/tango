pub mod annot_expression;
pub mod annot_statement;
pub mod ty;

use crate::ast::expression::*;
use crate::ast::statement::{Statement, StatementKind};
use crate::environment::Environment;
use crate::location::Location;
use crate::token::{Token, TokenKind};

use annot_expression::*;
use annot_statement::AnnotStatement;
use ty::Ty;

#[derive(Debug, Clone)]
pub struct Annotator {
    filename: String,
    environment: Environment<String, Ty>,
    diagnostics: Vec<String>,
}

impl Annotator {
    pub fn new(filename: String) -> Self {
        Self {
            filename,
            environment: Environment::new(),
            diagnostics: Vec::new(),
        }
    }

    pub fn annotate(&mut self, statement: &Statement) -> Result<AnnotStatement, Vec<String>> {
        let stmt = self.annotate_statement(statement);
        if self.diagnostics.is_empty() {
            Ok(stmt)
        } else {
            Err(std::mem::take(&mut self.diagnostics))
        }
    }

    fn annotate_statement(&mut self, statement: &Statement) -> AnnotStatement {
        match statement.kind() {
            StatementKind::Expression { expression } => {
                AnnotStatement::make_statement_expression(self.annotate_expression(expression))
            }
            StatementKind::VariableDeclaration { name, initializer } => {
                let initializer = match initializer
                    .as_ref()
                    .map(|init| self.annotate_expression(init))
                {
                    Some(initializer) => initializer,
                    None => {
                        // TODO: allow variable declaration without initializer but instead with type signature
                        self.error_at(
                            statement.location(),
                            "currently, variable declarations must have an initializer",
                        );

                        // HACK
                        return AnnotStatement::make_variable_declaration(
                            AnnotVariable::new(Ty::TypeError, name.lexeme.clone()),
                            None,
                        );
                    }
                };

                let variable = self.annotate_variable(name, &initializer);

                if variable.ty() != &Ty::TypeError {
                    self.environment.add(variable.name().into(), *variable.ty());
                }

                AnnotStatement::make_variable_declaration(variable, Some(initializer))
            }
            StatementKind::If {
                condition,
                then,
                elze,
            } => {
                let ann_condition = self.annotate_expression(condition);
                if ann_condition.ty() != Ty::Bool {
                    self.type_error(condition.location(), ann_condition.ty(), Ty::Bool);
                }

                let ann_then = self.annotate_block(then);
                let ann_elze = self.annotate_block(elze);
                AnnotStatement::make_if_statement(ann_condition, ann_then, ann_elze)
            }
            StatementKind::While { condition, body } => {
                let ann_condition = self.annotate_expression(condition);
                if ann_condition.ty() != Ty::Bool {
                    self.type_error(condition.location(), ann_condition.ty(), Ty::Bool);
                }

                let ann_body = self.annotate_block(body);
                AnnotStatement::make_while_statement(ann_condition, ann_body)
            }
        }
    }

    fn annotate_block(&mut self, block: &[Statement]) -> Vec<AnnotStatement> {
        self.environment.new_scope();
        let ann_block = block
            .iter()
            .map(|stmt| self.annotate_statement(stmt))
            .collect();
        self.environment.close_scope();
        ann_block
    }

    fn annotate_variable(&mut self, name: &Token, init: &AnnotExpression) -> AnnotVariable {
        AnnotVariable::new(init.ty(), name.lexeme.clone())
    }

    fn annotate_expression(&mut self, expression: &Expression) -> AnnotExpression {
        match expression.kind() {
            ExpressionKind::Literal(token) => match token.kind {
                TokenKind::Number(v) => AnnotExpression::make_i64_literal_expression(v),
                TokenKind::KwTrue => AnnotExpression::make_bool_literal_expression(true),
                TokenKind::KwFalse => AnnotExpression::make_bool_literal_expression(false),
                _ => unreachable!(),
            },
            ExpressionKind::Variable(token) => {
                let ty = match self.environment.get(&token.lexeme) {
                    Some(ty) => *ty,
                    None => {
                        self.error_at(
                            token.location,
                            format!("undefined variable `{}`", token.lexeme),
                        );
                        Ty::TypeError
                    }
                };
                AnnotExpression::make_variable_expression(token.lexeme.clone(), ty)
            }
            ExpressionKind::Assignment { target, value } => {
                let ann_value = self.annotate_expression(value);
                let ty = match self.environment.get(&target.lexeme) {
                    Some(ty) => *ty,
                    None => {
                        self.error_at(
                            target.location,
                            format!("undefined variable `{}`", target.lexeme),
                        );
                        Ty::TypeError
                    }
                };
                if ty != ann_value.ty() {
                    self.type_error(value.location(), ann_value.ty(), ty);
                }
                AnnotExpression::make_assignment_expression(target.lexeme.clone(), ann_value)
            }
            ExpressionKind::UnaryOperation { operator, right } => {
                let ann_right = self.annotate_expression(right);
                let op_str = &operator.lexeme;

                let ann_operator = match operator.kind {
                    TokenKind::Minus => AnnotUnaryOperator::Negation,
                    _ => unreachable!(),
                };

                let out_ty = match ann_operator.validate_types(ann_right.ty()) {
                    Some(out_ty) => out_ty,
                    None => {
                        self.type_error_operator(operator.location, op_str, &[ann_right.ty()]);
                        Ty::TypeError
                    }
                };
                AnnotExpression::make_unary_operation(out_ty, ann_operator, ann_right)
            }
            ExpressionKind::BinaryOperation {
                left,
                operator,
                right,
            } => {
                let ann_left = self.annotate_expression(left);
                let ann_right = self.annotate_expression(right);
                let op_str = &operator.lexeme;

                let ann_operator = match operator.kind {
                    TokenKind::Plus => AnnotBinaryOperator::Addition,
                    TokenKind::Minus => AnnotBinaryOperator::Subtraction,
                    TokenKind::Star => AnnotBinaryOperator::Multiplication,
                    TokenKind::Slash => AnnotBinaryOperator::Division,

                    TokenKind::Greater => AnnotBinaryOperator::GreaterThan,
                    TokenKind::GreaterEqual => AnnotBinaryOperator::GreaterOrEqual,
                    TokenKind::Less => AnnotBinaryOperator::LessThan,
                    TokenKind::LessEqual => AnnotBinaryOperator::LessOrEqual,
                    TokenKind::EqualEqual => AnnotBinaryOperator::Equality,
                    TokenKind::BangEqual => AnnotBinaryOperator::Inequality,
                    _ => unreachable!(),
                };

                let out_ty = match ann_operator.validate_types(ann_left.ty(), ann_right.ty()) {
                    Some(out_ty) => out_ty,
                    None => {
                        self.type_error_operator(
                            operator.location,
                            op_str,
                            &[ann_left.ty(), ann_right.ty()],
                        );
                        Ty::TypeError
                    }
                };
                AnnotExpression::make_binary_operation(out_ty, ann_left, ann_operator, ann_right)
            }
        }
    }

    fn type_error(&mut self, location: Location, ty_got: Ty, ty_expected: Ty) {
        if ty_got == Ty::TypeError || ty_expected == Ty::TypeError {
            return;
        }

        self.error_at(
            location,
            format!(
                "expected type `{}` but got `{}`",
                ty_expected.name(),
                ty_got.name()
            ),
        )
    }

    fn type_error_operator(&mut self, location: Location, operator: &str, ty_got: &[Ty]) {
        if ty_got.contains(&Ty::TypeError) {
            return;
        }

        let types = ty_got
            .iter()
            .map(|ty| ty.name())
            .map(|name| format!("`{}`", name))
            .collect::<Vec<_>>()
            .join(", ");

        self.error_at(
            location,
            format!(
                "operator `{}` is not defined for type{} {}",
                operator,
                if ty_got.len() > 1 { "s" } else { "" },
                types,
            ),
        )
    }

    fn error_at<T: std::fmt::Display>(&mut self, location: Location, msg: T) {
        let diagnostic = format!("{}:{}: ERROR: {}", self.filename, location, msg);
        self.diagnostics.push(diagnostic);
    }
}
