use super::ty::Ty;

#[derive(Debug, Clone)]
pub struct AnnotExpression {
    ty: Ty,
    kind: AnnotExpressionKind,
}

impl AnnotExpression {
    pub fn make_i64_literal_expression(v: i64) -> Self {
        Self {
            ty: Ty::I64,
            kind: AnnotExpressionKind::Literal(AnnotLiteral::I64(v)),
        }
    }

    pub fn make_bool_literal_expression(v: bool) -> Self {
        Self {
            ty: Ty::Bool,
            kind: AnnotExpressionKind::Literal(AnnotLiteral::Bool(v)),
        }
    }

    pub fn make_variable_expression(name: String, ty: Ty) -> Self {
        Self {
            ty,
            kind: AnnotExpressionKind::Variable(AnnotVariable { ty, name }),
        }
    }

    pub fn make_assignment_expression(name: String, value: AnnotExpression) -> Self {
        Self {
            ty: Ty::Unit,
            kind: AnnotExpressionKind::Assignment {
                name,
                value: Box::new(value),
            },
        }
    }

    pub fn make_unary_operation(
        ty: Ty,
        operator: AnnotUnaryOperator,
        right: AnnotExpression,
    ) -> Self {
        Self {
            ty,
            kind: AnnotExpressionKind::UnaryOperation {
                operator,
                right: Box::new(right),
            },
        }
    }

    pub fn make_binary_operation(
        ty: Ty,
        left: AnnotExpression,
        operator: AnnotBinaryOperator,
        right: AnnotExpression,
    ) -> Self {
        Self {
            ty,
            kind: AnnotExpressionKind::BinaryOperation {
                left: Box::new(left),
                operator,
                right: Box::new(right),
            },
        }
    }

    pub fn ty(&self) -> Ty {
        self.ty
    }

    pub fn kind(&self) -> &AnnotExpressionKind {
        &self.kind
    }
}

#[derive(Debug, Clone)]
pub struct AnnotVariable {
    ty: Ty,
    name: String,
}

impl AnnotVariable {
    pub fn new(ty: Ty, name: String) -> Self {
        Self { ty, name }
    }

    pub fn ty(&self) -> &Ty {
        &self.ty
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

#[derive(Debug, Clone)]
pub enum AnnotExpressionKind {
    Literal(AnnotLiteral),
    Variable(AnnotVariable),
    Assignment {
        name: String,
        value: Box<AnnotExpression>,
    },
    UnaryOperation {
        operator: AnnotUnaryOperator,
        right: Box<AnnotExpression>,
    },
    BinaryOperation {
        left: Box<AnnotExpression>,
        operator: AnnotBinaryOperator,
        right: Box<AnnotExpression>,
    },
}

#[derive(Debug, Clone)]
pub enum AnnotLiteral {
    I64(i64),
    Bool(bool),
}

#[derive(Debug, Clone)]
pub enum AnnotBinaryOperator {
    Addition,
    Subtraction,
    Multiplication,
    Division,
    LessThan,
    LessOrEqual,
    GreaterThan,
    GreaterOrEqual,
    Equality,
    Inequality,
}

impl AnnotBinaryOperator {
    const ADDITION_VALID_TYPES: &'static [((Ty, Ty), Ty)] = &[((Ty::I64, Ty::I64), Ty::I64)];

    const SUBTRACTION_VALID_TYPES: &'static [((Ty, Ty), Ty)] = &[((Ty::I64, Ty::I64), Ty::I64)];

    const MULTIPLICATION_VALID_TYPES: &'static [((Ty, Ty), Ty)] = &[((Ty::I64, Ty::I64), Ty::I64)];

    const DIVISION_VALID_TYPES: &'static [((Ty, Ty), Ty)] = &[((Ty::I64, Ty::I64), Ty::I64)];

    const COMPARISON_VALID_TYPES: &'static [((Ty, Ty), Ty)] = &[((Ty::I64, Ty::I64), Ty::Bool)];

    const EQUALITY_VALID_TYPES: &'static [((Ty, Ty), Ty)] = &[
        ((Ty::I64, Ty::I64), Ty::Bool),
        ((Ty::Bool, Ty::Bool), Ty::Bool),
        ((Ty::Unit, Ty::Unit), Ty::Bool),
    ];

    pub fn validate_types(&self, left: Ty, right: Ty) -> Option<Ty> {
        let types = match self {
            Self::Addition => Self::ADDITION_VALID_TYPES,
            Self::Subtraction => Self::SUBTRACTION_VALID_TYPES,
            Self::Multiplication => Self::MULTIPLICATION_VALID_TYPES,
            Self::Division => Self::DIVISION_VALID_TYPES,

            Self::LessThan => Self::COMPARISON_VALID_TYPES,
            Self::LessOrEqual => Self::COMPARISON_VALID_TYPES,
            Self::GreaterThan => Self::COMPARISON_VALID_TYPES,
            Self::GreaterOrEqual => Self::COMPARISON_VALID_TYPES,
            Self::Equality => Self::EQUALITY_VALID_TYPES,
            Self::Inequality => Self::EQUALITY_VALID_TYPES,
        };
        for (in_ty, out_ty) in types {
            if (left, right) == *in_ty {
                return Some(*out_ty);
            }
        }
        None
    }
}

#[derive(Debug, Clone)]
pub enum AnnotUnaryOperator {
    Negation,
}

impl AnnotUnaryOperator {
    const NEGATION_VALID_TYPES: [(Ty, Ty); 1] = [(Ty::I64, Ty::I64)];

    pub fn validate_types(&self, right: Ty) -> Option<Ty> {
        let types = match self {
            Self::Negation => Self::NEGATION_VALID_TYPES,
        };
        for (in_ty, out_ty) in types {
            if right == in_ty {
                return Some(out_ty);
            }
        }
        None
    }
}
