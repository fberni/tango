#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Ty {
    Bool,
    I64,
    Unit,
    TypeError,
}

impl Ty {
    pub fn name(&self) -> &str {
        match self {
            Self::Bool => "bool",
            Self::I64 => "i64",
            Self::Unit => "()",
            Self::TypeError => "<type error>",
        }
    }
}
