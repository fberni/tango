use super::annot_expression::{AnnotExpression, AnnotVariable};

#[derive(Debug, Clone)]
pub struct AnnotStatement {
    kind: AnnotStatementKind,
}

impl AnnotStatement {
    pub fn make_statement_expression(expression: AnnotExpression) -> Self {
        Self {
            kind: AnnotStatementKind::Expression { expression },
        }
    }

    pub fn make_variable_declaration(
        variable: AnnotVariable,
        initializer: Option<AnnotExpression>,
    ) -> Self {
        Self {
            kind: AnnotStatementKind::VariableDeclaration {
                variable,
                initializer,
            },
        }
    }

    pub fn make_if_statement(
        condition: AnnotExpression,
        then: Vec<AnnotStatement>,
        elze: Vec<AnnotStatement>,
    ) -> Self {
        Self {
            kind: AnnotStatementKind::If {
                condition,
                then,
                elze,
            },
        }
    }

    pub fn make_while_statement(condition: AnnotExpression, body: Vec<AnnotStatement>) -> Self {
        Self {
            kind: AnnotStatementKind::While { condition, body },
        }
    }

    pub fn kind(&self) -> &AnnotStatementKind {
        &self.kind
    }
}

#[derive(Debug, Clone)]
pub enum AnnotStatementKind {
    Expression {
        expression: AnnotExpression,
    },
    VariableDeclaration {
        variable: AnnotVariable,
        initializer: Option<AnnotExpression>,
    },
    If {
        condition: AnnotExpression,
        then: Vec<AnnotStatement>,
        elze: Vec<AnnotStatement>,
    },
    While {
        condition: AnnotExpression,
        body: Vec<AnnotStatement>,
    },
}
