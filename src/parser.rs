use crate::ast::expression::{Expression, ExpressionKind};
use crate::ast::statement::{Statement, StatementKind};
use crate::lexer::Lexer;
use crate::location::Location;
use crate::token::{Token, TokenKind};

#[derive(Debug, Clone)]
pub struct Parser {
    filename: String,
    tokens: Vec<Token>,
    offset: usize,
}

type ParserResult<T> = Result<T, Vec<String>>;

impl Parser {
    pub fn new(filename: String, input: String) -> ParserResult<Self> {
        let mut lexer = Lexer::new(&input);
        let mut tokens = Vec::new();
        let mut diagnostics = Vec::new();

        let mut token = lexer.next_token();
        loop {
            match token.kind {
                TokenKind::Invalid => {
                    diagnostics.push(format!(
                        "{}:{} ERROR: invalid character `{}`",
                        filename, token.location, token.lexeme
                    ));
                }
                TokenKind::Eof => {
                    tokens.push(token);
                    break;
                }
                _ => tokens.push(token),
            }

            token = lexer.next_token();
        }

        if diagnostics.is_empty() {
            Ok(Self {
                filename,
                tokens,
                offset: 0,
            })
        } else {
            Err(diagnostics)
        }
    }

    /// Parses a statement block without an opening brace
    fn parse_block_no_open_brace(&mut self) -> ParserResult<Vec<Statement>> {
        let mut statements = Vec::new();
        while self.current().kind != TokenKind::RightBrace && self.current().kind != TokenKind::Eof
        {
            statements.push(self.parse_statement()?);
        }
        self.consume(TokenKind::RightBrace, "unclosed brace")?;
        Ok(statements)
    }

    /// Parses any statement
    pub fn parse_statement(&mut self) -> ParserResult<Statement> {
        match self.current().kind {
            TokenKind::KwLet => self.parse_variable_declaration(),
            TokenKind::KwIf => self.parse_if_statement(),
            TokenKind::KwWhile => self.parse_while_statement(),
            _ => self.parse_statement_expression(),
        }
    }

    /// Parses a while statement
    fn parse_while_statement(&mut self) -> ParserResult<Statement> {
        let while_tok = self.advance().clone();
        assert_eq!(while_tok.kind, TokenKind::KwWhile);

        let condition = self.parse_expression()?;
        self.consume(TokenKind::LeftBrace, "expected `{` after while condition")?;
        let body = self.parse_block_no_open_brace()?;

        Ok(Statement::new(
            while_tok.location,
            StatementKind::While { condition, body },
        ))
    }

    /// Parses an if statement
    fn parse_if_statement(&mut self) -> ParserResult<Statement> {
        let if_tok = self.advance().clone();
        assert_eq!(if_tok.kind, TokenKind::KwIf);

        let condition = self.parse_expression()?;
        self.consume(TokenKind::LeftBrace, "expected `{` after if condition")?;
        let then = self.parse_block_no_open_brace()?;

        let elze = if self.current().kind == TokenKind::KwElse {
            self.advance();
            if self.current().kind == TokenKind::KwIf {
                vec![self.parse_if_statement()?]
            } else {
                self.consume(TokenKind::LeftBrace, "expected `{` after else")?;
                self.parse_block_no_open_brace()?
            }
        } else {
            Vec::new()
        };

        Ok(Statement::new(
            if_tok.location,
            StatementKind::If {
                condition,
                then,
                elze,
            },
        ))
    }

    /// Parses a variable declaration of the form
    /// `"let" <ident> ( "=" <expression> )? ";"`
    fn parse_variable_declaration(&mut self) -> ParserResult<Statement> {
        let let_tok = self.advance();
        assert_eq!(let_tok.kind, TokenKind::KwLet);
        let location = let_tok.location;

        let name = self
            .consume(TokenKind::Identifier, "expected variable name")?
            .clone();

        let initializer = if self.current().kind == TokenKind::Equal {
            self.advance();
            Some(self.parse_expression()?)
        } else {
            None
        };

        self.consume(
            TokenKind::Semicolon,
            "expected semicolon after variable declaration",
        )?;

        Ok(Statement::new(
            location,
            StatementKind::VariableDeclaration { name, initializer },
        ))
    }

    /// Parses a statement expression
    fn parse_statement_expression(&mut self) -> ParserResult<Statement> {
        let expression = self.parse_expression()?;
        self.consume(TokenKind::Semicolon, "expected `;` after expression")?;
        Ok(Statement::new(
            expression.location(),
            StatementKind::Expression { expression },
        ))
    }

    /// Parses any expression
    fn parse_expression(&mut self) -> ParserResult<Expression> {
        self.parse_assignment_expression()
    }

    /// Parses an assignment expression
    ///
    /// This will parse an a binary expression resulting in an lvalue followed
    /// by an equals sign and an expression, or just a binary expression
    ///
    /// An lvalue is anything that can be assigned to, so currently only a
    /// variable reference.
    fn parse_assignment_expression(&mut self) -> ParserResult<Expression> {
        let lhs = self.parse_unary_expression()?;
        let expression = self.parse_binary_expression(lhs, 0)?;

        if self.current().kind == TokenKind::Equal {
            self.advance();
            let value = self.parse_expression()?;

            if let ExpressionKind::Variable(target) = expression.kind() {
                return Ok(Expression::new(
                    expression.location(),
                    ExpressionKind::Assignment {
                        target: target.clone(),
                        value: Box::new(value),
                    },
                ));
            }

            return Err(vec![
                self.error_at(expression.location(), "invalid assignment target")
            ]);
        }

        Ok(expression)
    }

    /// Parses binary operations with at least `minimum_precedence` using
    /// [operator-precedence parsing](https://en.wikipedia.org/wiki/Operator-precedence_parser)
    fn parse_binary_expression(
        &mut self,
        mut left: Expression,
        minimum_precedence: i32,
    ) -> ParserResult<Expression> {
        let location = left.location();
        while self.current().kind.precedence() >= minimum_precedence {
            let operator = self.advance().clone();
            let operator_precedence = operator.kind.precedence();
            let mut right = self.parse_unary_expression()?;

            while self.current().kind.precedence() > operator_precedence {
                right = self.parse_binary_expression(right, operator_precedence + 1)?;
            }

            left = Expression::new(
                location,
                ExpressionKind::BinaryOperation {
                    left: Box::new(left),
                    right: Box::new(right),
                    operator,
                },
            )
        }
        Ok(left)
    }

    /// Parses a unary expression
    ///
    /// This will parse a `-` followed by another unary expression,
    /// or a primary expression
    fn parse_unary_expression(&mut self) -> ParserResult<Expression> {
        match self.current().kind {
            TokenKind::Minus => {
                let operator = self.advance().clone();
                let right = self.parse_unary_expression()?;
                Ok(Expression::new(
                    operator.location,
                    ExpressionKind::UnaryOperation {
                        operator,
                        right: Box::new(right),
                    },
                ))
            }
            _ => self.parse_primary_expression(),
        }
    }

    /// Parses a primary expression
    ///
    /// A primary expression consists of a literal, a parenthesized expression
    /// or an identifier
    fn parse_primary_expression(&mut self) -> ParserResult<Expression> {
        match self.current().kind {
            TokenKind::Number(..) | TokenKind::KwTrue | TokenKind::KwFalse => {
                let token = self.advance();
                Ok(Expression::new(
                    token.location,
                    ExpressionKind::Literal(token.clone()),
                ))
            }
            TokenKind::LeftParen => {
                self.advance();
                let expression = self.parse_expression()?;
                self.consume(
                    TokenKind::RightParen,
                    "unclosed parenthesis after expression",
                )?;
                Ok(expression)
            }
            TokenKind::Identifier => {
                let token = self.advance();
                Ok(Expression::new(
                    token.location,
                    ExpressionKind::Variable(token.clone()),
                ))
            }
            _ => Err(vec![self.error_at_current("expected expression")]),
        }
    }

    fn consume<T: std::fmt::Display>(&mut self, kind: TokenKind, msg: T) -> ParserResult<&Token> {
        if self.current().kind == kind {
            Ok(self.advance())
        } else {
            Err(vec![self.error_at_current(msg)])
        }
    }

    fn current(&self) -> &Token {
        if self.offset >= self.tokens.len() {
            self.tokens.last().unwrap()
        } else {
            &self.tokens[self.offset]
        }
    }

    fn advance(&mut self) -> &Token {
        let prev_offset = self.offset;
        if self.offset < self.tokens.len() {
            self.offset += 1;
        }
        &self.tokens[prev_offset]
    }

    fn error_at_current<T: std::fmt::Display>(&mut self, msg: T) -> String {
        if self.current().kind == TokenKind::Eof {
            format!(
                "{}:{}: ERROR: {} at end of file",
                self.filename,
                self.current().location,
                msg,
            )
        } else {
            format!(
                "{}:{}: ERROR: {} at `{}`",
                self.filename,
                self.current().location,
                msg,
                self.current().lexeme,
            )
        }
    }

    fn error_at<T: std::fmt::Display>(&mut self, location: Location, msg: T) -> String {
        format!("{}:{}: ERROR: {}", self.filename, location, msg,)
    }
}
