use crate::location::Location;
use crate::token::Token;

use super::expression::Expression;

#[derive(Debug, Clone)]
pub struct Statement {
    location: Location,
    kind: StatementKind,
}

impl Statement {
    pub fn new(location: Location, kind: StatementKind) -> Self {
        Self { location, kind }
    }

    pub fn kind(&self) -> &StatementKind {
        &self.kind
    }

    pub fn location(&self) -> Location {
        self.location
    }
}

#[derive(Debug, Clone)]
pub enum StatementKind {
    Expression {
        expression: Expression,
    },
    VariableDeclaration {
        name: Token,
        initializer: Option<Expression>,
    },
    If {
        condition: Expression,
        then: Vec<Statement>,
        elze: Vec<Statement>,
    },
    While {
        condition: Expression,
        body: Vec<Statement>,
    },
}
