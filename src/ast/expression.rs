use crate::location::Location;
use crate::token::Token;

#[derive(Debug, Clone)]
pub struct Expression {
    location: Location,
    kind: ExpressionKind,
}

impl Expression {
    pub fn new(location: Location, kind: ExpressionKind) -> Self {
        Self { location, kind }
    }

    pub fn location(&self) -> Location {
        self.location
    }

    pub fn kind(&self) -> &ExpressionKind {
        &self.kind
    }
}

#[derive(Debug, Clone)]
pub enum ExpressionKind {
    Literal(Token),
    Variable(Token),
    Assignment {
        target: Token,
        value: Box<Expression>,
    },
    UnaryOperation {
        operator: Token,
        right: Box<Expression>,
    },
    BinaryOperation {
        left: Box<Expression>,
        operator: Token,
        right: Box<Expression>,
    },
}
