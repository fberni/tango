mod annot;
mod ast;
mod environment;
mod evaluator;
mod lexer;
mod location;
mod parser;
mod token;

use std::io::{self, BufRead, Write};

use annot::Annotator;
use evaluator::Evaluator;
use parser::Parser;

fn repl() {
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    let mut show_ast = false;

    println!("Welcome to the Tango REPL!");
    println!();
    println!("Try writing an expression like `34 + 5;`");
    println!("To exit, press Ctrl + D");
    println!();

    let mut evaluator = Evaluator::new();
    let mut annotator = Annotator::new("<repl>".into());

    loop {
        print!("> ");
        if io::stdout().flush().is_err() {
            break;
        }

        let mut line = String::new();
        if handle.read_line(&mut line).unwrap_or_default() == 0 {
            break;
        }

        match line.trim() {
            line if line.is_empty() => continue,
            "#ast" => {
                show_ast = !show_ast;
                println!("Show AST: {}", if show_ast { "on" } else { "off" });
                continue;
            }
            _ => {}
        }

        let mut parser = match Parser::new("<repl>".into(), line) {
            Ok(parser) => parser,
            Err(diagnostics) => {
                for diag in diagnostics {
                    eprintln!("{}", diag);
                }
                continue;
            }
        };

        let parse_tree = match parser.parse_statement() {
            Ok(parse_tree) => parse_tree,
            Err(diagnostics) => {
                for diag in diagnostics {
                    eprintln!("{}", diag);
                }
                continue;
            }
        };

        let annot_tree = match annotator.annotate(&parse_tree) {
            Ok(annot_tree) => annot_tree,
            Err(diagnostics) => {
                for diag in diagnostics {
                    eprintln!("{}", diag);
                }
                continue;
            }
        };

        if show_ast {
            println!("{:#?}", annot_tree);
        }

        match evaluator.evaluate_statement(&annot_tree) {
            Ok(result) => println!("{}", result),
            Err(err) => eprintln!("{}", err),
        };
    }
}

fn main() {
    repl()
}
