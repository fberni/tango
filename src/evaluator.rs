use crate::annot::annot_expression::*;
use crate::annot::annot_statement::{AnnotStatement, AnnotStatementKind};
use crate::environment::Environment;

use std::fmt;

#[derive(Debug, Clone)]
pub enum Value {
    I64(i64),
    Bool(bool),
    Unit,
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::I64(v) => v.fmt(f),
            Self::Bool(v) => v.fmt(f),
            Self::Unit => write!(f, "()"),
        }
    }
}

impl From<bool> for Value {
    fn from(v: bool) -> Self {
        Self::Bool(v)
    }
}
impl From<i64> for Value {
    fn from(v: i64) -> Self {
        Self::I64(v)
    }
}
impl From<()> for Value {
    fn from(_: ()) -> Self {
        Self::Unit
    }
}

impl TryFrom<Value> for i64 {
    type Error = &'static str;

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        match value {
            Value::I64(v) => Ok(v),
            _ => Err("invalid type cast"),
        }
    }
}
impl TryFrom<Value> for bool {
    type Error = &'static str;

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        match value {
            Value::Bool(v) => Ok(v),
            _ => Err("invalid type cast"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Evaluator {
    environment: Environment<String, Value>,
}

type EvalResult<T> = Result<T, String>;

impl Evaluator {
    pub fn new() -> Self {
        Self {
            environment: Environment::new(),
        }
    }

    pub fn evaluate_block(&mut self, statements: &[AnnotStatement]) -> EvalResult<Value> {
        self.environment.new_scope();
        let result = statements
            .iter()
            .try_fold(Value::Unit, |_, stmt| self.evaluate_statement(stmt));
        self.environment.close_scope();
        result
    }

    pub fn evaluate_statement(&mut self, statement: &AnnotStatement) -> EvalResult<Value> {
        match statement.kind() {
            AnnotStatementKind::Expression { expression } => self.evaluate_expression(expression),
            AnnotStatementKind::VariableDeclaration {
                variable,
                initializer,
            } => {
                let name = variable.name();
                let value = self.evaluate_expression(initializer.as_ref().unwrap())?;
                self.environment.add(name.into(), value);
                Ok(().into())
            }
            AnnotStatementKind::If {
                condition,
                then,
                elze,
            } => {
                if self.evaluate_expression(condition)?.try_into().unwrap() {
                    self.evaluate_block(then)
                } else {
                    self.evaluate_block(elze)
                }
            }
            AnnotStatementKind::While { condition, body } => {
                let mut result = Value::Unit;
                while self.evaluate_expression(condition)?.try_into().unwrap() {
                    result = self.evaluate_block(body)?;
                }
                Ok(result)
            }
        }
    }

    fn evaluate_expression(&mut self, expression: &AnnotExpression) -> EvalResult<Value> {
        match expression.kind() {
            AnnotExpressionKind::Literal(value) => match value {
                AnnotLiteral::I64(v) => Ok((*v).into()),
                AnnotLiteral::Bool(v) => Ok((*v).into()),
            },
            AnnotExpressionKind::Variable(variable) => {
                Ok(self.environment.get(variable.name()).unwrap().clone())
            }
            AnnotExpressionKind::Assignment { name, value } => {
                let value = self.evaluate_expression(value)?;
                self.environment.set(name.into(), value);
                Ok(().into())
            }
            AnnotExpressionKind::UnaryOperation { operator, right } => {
                let right: i64 = self.evaluate_expression(right)?.try_into().unwrap();

                match operator {
                    AnnotUnaryOperator::Negation => Ok((-right).into()),
                }
            }
            AnnotExpressionKind::BinaryOperation {
                left,
                operator,
                right,
            } => match operator {
                AnnotBinaryOperator::Equality | AnnotBinaryOperator::Inequality => {
                    match self.evaluate_expression(left)? {
                        Value::Bool(v) => {
                            let right: bool = self.evaluate_expression(right)?.try_into().unwrap();
                            match operator {
                                AnnotBinaryOperator::Equality => Ok((v == right).into()),
                                AnnotBinaryOperator::Inequality => Ok((v != right).into()),
                                _ => unreachable!(),
                            }
                        }
                        Value::I64(v) => {
                            let right: i64 = self.evaluate_expression(right)?.try_into().unwrap();
                            match operator {
                                AnnotBinaryOperator::Equality => Ok((v == right).into()),
                                AnnotBinaryOperator::Inequality => Ok((v != right).into()),
                                _ => unreachable!(),
                            }
                        }
                        Value::Unit => {
                            self.evaluate_expression(right)?;
                            match operator {
                                AnnotBinaryOperator::Equality => Ok(true.into()),
                                AnnotBinaryOperator::Inequality => Ok(false.into()),
                                _ => unreachable!(),
                            }
                        }
                    }
                }
                _ => {
                    let left: i64 = self.evaluate_expression(left)?.try_into().unwrap();
                    let right: i64 = self.evaluate_expression(right)?.try_into().unwrap();

                    match operator {
                        AnnotBinaryOperator::Addition => Ok((left + right).into()),
                        AnnotBinaryOperator::Subtraction => Ok((left - right).into()),
                        AnnotBinaryOperator::Multiplication => Ok((left * right).into()),
                        AnnotBinaryOperator::GreaterThan => Ok((left > right).into()),
                        AnnotBinaryOperator::GreaterOrEqual => Ok((left >= right).into()),
                        AnnotBinaryOperator::LessThan => Ok((left < right).into()),
                        AnnotBinaryOperator::LessOrEqual => Ok((left <= right).into()),
                        AnnotBinaryOperator::Division => {
                            if right == 0 {
                                Err("ERROR: division by zero!".into())
                            } else {
                                Ok((left / right).into())
                            }
                        }
                        _ => unreachable!(),
                    }
                }
            },
        }
    }
}
