use crate::location::Location;
use crate::token::{Token, TokenKind};

#[derive(Debug, Clone)]
pub struct Lexer {
    input: Vec<char>,
    location: Location,
}

impl Lexer {
    pub fn new(input: &str) -> Self {
        Self {
            input: input.chars().collect(),
            location: Location::default(),
        }
    }

    pub fn next_token(&mut self) -> Token {
        loop {
            let start = self.location;
            match self.current() {
                '+' => {
                    self.advance();
                    break Token::new(TokenKind::Plus, "+".into(), start);
                }
                '-' => {
                    self.advance();
                    break Token::new(TokenKind::Minus, "-".into(), start);
                }
                '*' => {
                    self.advance();
                    break Token::new(TokenKind::Star, "*".into(), start);
                }
                '/' => {
                    self.advance();
                    break Token::new(TokenKind::Slash, "/".into(), start);
                }
                '(' => {
                    self.advance();
                    break Token::new(TokenKind::LeftParen, "(".into(), start);
                }
                ')' => {
                    self.advance();
                    break Token::new(TokenKind::RightParen, ")".into(), start);
                }
                '{' => {
                    self.advance();
                    break Token::new(TokenKind::LeftBrace, "{".into(), start);
                }
                '}' => {
                    self.advance();
                    break Token::new(TokenKind::RightBrace, "}".into(), start);
                }
                ';' => {
                    self.advance();
                    break Token::new(TokenKind::Semicolon, ";".into(), start);
                }
                '>' => {
                    self.advance();
                    if self.current() == '=' {
                        self.advance();
                        break Token::new(TokenKind::GreaterEqual, ">=".into(), start);
                    } else {
                        break Token::new(TokenKind::Greater, ">".into(), start);
                    }
                }
                '<' => {
                    self.advance();
                    if self.current() == '=' {
                        self.advance();
                        break Token::new(TokenKind::LessEqual, "<=".into(), start);
                    } else {
                        break Token::new(TokenKind::Less, "<".into(), start);
                    }
                }
                '=' => {
                    self.advance();
                    if self.current() == '=' {
                        self.advance();
                        break Token::new(TokenKind::EqualEqual, "==".into(), start);
                    } else {
                        break Token::new(TokenKind::Equal, "=".into(), start);
                    }
                }
                '!' => {
                    self.advance();
                    if self.current() == '=' {
                        self.advance();
                        break Token::new(TokenKind::BangEqual, "!=".into(), start);
                    } else {
                        break Token::new(TokenKind::Bang, "!".into(), start);
                    }
                }
                c if c.is_digit(10) => {
                    let mut number = c.to_digit(10).unwrap() as i64;
                    self.advance();

                    while self.current().is_digit(10) {
                        number = number * 10 + self.current().to_digit(10).unwrap() as i64;
                        self.advance()
                    }
                    break Token::new(
                        TokenKind::Number(number),
                        String::from_iter(&self.input[start.offset..self.location.offset]),
                        start,
                    );
                }
                c if Self::is_keyword_start(c) => {
                    self.advance();
                    while Self::is_keyword_continue(self.current()) {
                        self.advance()
                    }
                    let lexeme = String::from_iter(&self.input[start.offset..self.location.offset]);
                    break Token::new(TokenKind::make_ident_or_keyword(&lexeme), lexeme, start);
                }
                c if c.is_whitespace() => {
                    self.advance();
                    continue;
                }
                '\0' => {
                    self.advance();
                    break Token::new(TokenKind::Eof, "".into(), start);
                }
                c => {
                    self.advance();
                    break Token::new(TokenKind::Invalid, c.into(), start);
                }
            }
        }
    }

    /// Returs `true` if `c` can be the start of a keyword, which happens if it
    /// is alphabetic or an underscore
    fn is_keyword_start(c: char) -> bool {
        c.is_alphabetic() || c == '_'
    }

    /// Returs `true` if `c` can be in the middle of a keyword, which happens
    /// if it is alphabetic, an underscore or a number
    fn is_keyword_continue(c: char) -> bool {
        Self::is_keyword_start(c) || c.is_numeric()
    }

    fn current(&self) -> char {
        if self.location.offset < self.input.len() {
            self.input[self.location.offset]
        } else {
            '\0'
        }
    }

    fn advance(&mut self) {
        if self.location.offset < self.input.len() {
            if self.current() == '\n' {
                self.location.column = 0;
                self.location.line += 1;
            } else {
                self.location.column += 1;
            }
            self.location.offset += 1;
        }
    }
}
